// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SalesforceMobileSDKSPM2",
    platforms: [
        .iOS(.v13)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SalesforceMobileSDKSPM2",
            type: .static,
            targets: ["SalesforceMobileSDKProxyTarget"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .binaryTarget(name: "SalesforceSDKCommon",
                      path: "Sources/SalesforceSDKCommon.xcframework"),
        .binaryTarget(name: "SalesforceAnalytics",
                      path: "Sources/SalesforceAnalytics.xcframework"),
        .binaryTarget(name: "SalesforceSDKCore",
                      path: "Sources/SalesforceSDKCore.xcframework"),
        .target(name: "SalesforceMobileSDKProxyTarget",
                dependencies: [.target(name: "SalesforceSDKCommon"),
                               .target(name: "SalesforceAnalytics"),
                               .target(name: "SalesforceSDKCore")
                ],
                path: "Sources/Proxy"
        )
    ]
)
